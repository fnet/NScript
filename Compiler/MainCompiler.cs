﻿using BSF.BaseService.NScript.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace BSF.BaseService.NScript.Compiler
{
    public class MainCompiler : BaseCompiler
    {

        public override Core.CompilerResult DoCompiler(CompilerInfo compilerInfo)
        {
            compilerInfo.EnumCompilerMode = EnumCompilerMode.Main;
            Assembly assembly = new DynamicCompilerProvider().Compiler(compilerInfo);

            return new CompilerResult()
            {
                EnumCompilerMode = Core.EnumCompilerMode.Main,
                Assembly = assembly
            };
        }
    }
}
