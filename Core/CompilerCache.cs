﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSF.BaseService.NScript.Core
{
    public class CompilerCache
    {
        private  Dictionary<int, CompilerResult> _cache = new Dictionary<int, CompilerResult>();
        private  object _cacheLock = new object();

        public CompilerResult GetOrAdd(int hash,Func<CompilerResult> func)
        {
            if (_cache.ContainsKey(hash))
            {
                return _cache[hash];
            }
            else
            {
                lock (_cacheLock)
                {
                    var result = func.Invoke();
                    _cache.Add(hash, result);
                    return _cache[hash];
                }
            }
        }
        public void Clear()
        {
            lock (_cacheLock)
            {
                _cache.Clear();
            }
 
        }
    }
}
